import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LogoutComponent} from './logout/logout.component';
import {ReportesComponent} from './reportes/reportes.component';
import {AddUserComponent} from './add-user/add-user.component';
import {PuntosComponent} from './puntos/puntos.component';
import {PromotionComponent} from './promotion/promotion.component';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent},
  {path: 'reportes', component: ReportesComponent},
  {path: 'add', component: AddUserComponent},
  {path: 'main', component: DashboardComponent},
  {path: 'prom', component: PromotionComponent},
  {path: 'puntos', component: PuntosComponent},
  {path: '**', redirectTo: 'login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
