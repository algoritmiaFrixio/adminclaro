import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NotificationService} from '../shared/notification.service';
import {GLOBALService} from '../shared/global.service';
import {MzToastService} from 'ngx-materialize';

@Component({
  selector: 'app-puntos',
  templateUrl: './puntos.component.html',
  styleUrls: ['./puntos.component.scss']
})
export class PuntosComponent implements OnInit {
  public users = [];
  public user = [];

  constructor(private router: Router, private messagingService: NotificationService, private globalService: GLOBALService, private toastService: MzToastService) {
    if (localStorage.getItem('user') === null) {
      router.navigate(['/inicio']);
    }
    globalService.getUsers().subscribe((response) => {
        this.users = response.users;
        this.user = response.users;
      }
    );
  }

  ngOnInit() {
  }

  change(event) {
    event = event.toLowerCase();
    setTimeout(() => {
      this.users = [];
      this.user.forEach(e => {
        if (e.nombre.toLowerCase().indexOf(event) > -1 || e.local.toLowerCase().indexOf(event) > -1 || e.ciudad.toLowerCase().indexOf(event) > -1 || e.departamento.toLowerCase().indexOf(event) > -1) {
          this.users.push(e);
        }
      });
    });
  }

  redimir(id, puntos, total, redimidos) {
    total = total - redimidos;
    if (total < puntos) {
      this.toastService.show('Los puntos a redimir deben ser menores a los disponibles', 4000, 'red');
    } else {
      this.globalService.redimir(id, puntos).subscribe(response => {
        this.toastService.show(response.message, 4000, 'green');
        this.users = response.users;
      }, error => {
        this.toastService.show(error.error.message, 4000, 'red');
      });
    }
  }
}
