import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {GLOBALService} from '../shared/global.service';
import {Router} from '@angular/router';
import {MzToastService} from 'ngx-materialize';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  public addUserForm: FormGroup;

  constructor(private globalService: GLOBALService, private toastService: MzToastService) {
    this.addUserForm = new FormGroup({
      nombre: new FormControl('', Validators.required),
      local: new FormControl('', Validators.required),
      cedula: new FormControl('', Validators.required),
      telefono: new FormControl('', Validators.required),
      telefono_local: new FormControl('', Validators.required),
      ciudad: new FormControl('', Validators.required),
      departamento: new FormControl('', Validators.required),
      barrio: new FormControl('', Validators.required),
      direccion: new FormControl('', Validators.required),
    });
  }

  public add(form: FormGroup) {
    this.globalService.addUser(form.value).subscribe((response: any) => {
      this.toastService.show(response.message, 4000, 'green');
      this.addUserForm.reset();

    }, error1 => {
      this.toastService.show(error1.error.message, 4000, 'red');
    });
  }

  ngOnInit() {
  }

}
