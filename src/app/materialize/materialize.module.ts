import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MzButtonModule, MzDatepickerModule, MzInputModule, MzNavbarModule, MzSelectModule, MzSidenavModule} from 'ngx-materialize';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';


const data = [
  CommonModule,
  MzInputModule,
  MzButtonModule,
  FormsModule,
  HttpClientModule,
  MzDatepickerModule,
  ReactiveFormsModule,
  MzNavbarModule,
  MzSelectModule,
  MzSidenavModule
];

@NgModule({
  declarations: [],
  imports: data,
  exports: data
})
export class MaterializeModule {
}
