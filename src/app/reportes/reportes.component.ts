import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {MzToastService} from 'ngx-materialize';
import {GLOBALService} from '../shared/global.service';

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.scss']
})
export class ReportesComponent implements OnInit {
  public options: Pickadate.DateOptions = {
    clear: 'Limpiar', // Clear button text
    close: 'Seleccionar',    // Ok button text
    today: 'Hoy', // Today button text
    closeOnClear: true,
    closeOnSelect: false,
    format: 'dddd d mmmm yyyy',
    formatSubmit: 'yyyy/mm/dd',
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 1,
    firstDay: 1,
    max: new Date(),
    // tslint:disable-next-line:max-line-length
    monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
    weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
  };
  private optionsF = {year: 'numeric', month: 'long', day: 'numeric'};
  public ventas = [];

  public desde = new Date(); // use formatSubmit format to set datepicker value
  public hasta = new Date(); // use formatSubmit format to set datepicker value
  public estado;

  constructor(private router: Router, private toastService: MzToastService, private globalService: GLOBALService) {
    if (localStorage.getItem('user') === null) {
      router.navigate(['/inicio']);
    }
  }

  getReports() {
    this.estado = this.estado === undefined ? null : this.estado;
    this.globalService.getReportes(new Date(this.desde).toDateString(), new Date(this.hasta).toDateString(), this.estado).subscribe((response) => {
      this.ventas = response;
      this.ventas.forEach(e => {
        e.created_at = new Date(e.created_at).toLocaleDateString('es-ES', this.optionsF);
      });
    });
  }

  ngOnInit() {
  }

  change(id, value, venta) {
    this.globalService.updateEstado(id, value, venta).subscribe((response: any) => {
      localStorage.setItem('ventas', JSON.stringify(response.ventas));
      this.ventas = response.ventas;
      this.ventas.forEach(e => {
        e.created_at = new Date(e.created_at).toLocaleDateString('es-ES', this.optionsF);
      });
    }, error1 => {
      this.toastService.show(error1.error.message, 4000, 'red');
    });
  }

}
