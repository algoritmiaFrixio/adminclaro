import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NotificationService} from '../shared/notification.service';
import {GLOBALService} from '../shared/global.service';
import {MzToastService} from 'ngx-materialize';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public ventas;
  public services;
  public select;
  private options = {year: 'numeric', month: 'long', day: 'numeric'};

  constructor(private router: Router, private messagingService: NotificationService, private globalService: GLOBALService, private toastService: MzToastService) {
    if (localStorage.getItem('user') === null) {
      router.navigate(['/inicio']);
    }
    this.ventas = JSON.parse(localStorage.getItem('ventas'));
    globalService.getventas().subscribe((response) => {
        this.ventas = response;
        this.ventas.forEach(e => {
          if (e.service.nombre === 'Hogar') {
            e.service.puntos = 0;
          }
          e.created_at = new Date(e.created_at).toLocaleDateString('es-ES', this.options);
        });
      }
    );
  }

  change(id, value, venta) {
    if (venta.service.nombre === 'Hogar' && venta.service.puntos == 0 && value === 'Exitoso') {
      this.toastService.show('Por favor seleccione un tipo de servicio', 4000, 'red');
      return;
    }
    this.globalService.updateEstado(id, value, venta).subscribe((response: any) => {
      localStorage.setItem('ventas', JSON.stringify(response.ventas));
      this.ventas = response.ventas;
      this.ventas.forEach(e => {
        e.created_at = new Date(e.created_at).toLocaleDateString('es-ES', this.options);
        if (e.service.nombre === 'Hogar') {
          e.service.puntos = 0;
        }
      });
    }, error1 => {
      this.toastService.show(error1.error.message, 4000, 'red');
    });
  }

  changeType(index, value) {
    this.ventas[index].service.puntos = value;
  }

  ngOnInit() {
    const userId = JSON.parse(localStorage.getItem('user')).id;
    this.messagingService.requestPermission(userId);
    this.messagingService.receiveMessage();
  }

  public fileEvent($event) {
    const fileSelected: File = $event.target.files[0];
    this.globalService.uploadFile(fileSelected)
      .subscribe((response) => {
          console.log(response);
        },
        error => {
          console.error(error);
        });
  }
}
