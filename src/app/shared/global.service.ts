import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GLOBALService {

  constructor(private http: HttpClient) {
  }

  public login(data): Observable<any> {
    return this.http.post<any>(`${environment.url}authAdministrator`, data,
      {
        headers: {'Content-Type': 'application/json'},
        observe: 'response'
      }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  public getventas(): Observable<any> {
    return this.http.get<any>(`${environment.url}ventas`,
      {
        headers: {'Content-Type': 'application/json'},
        observe: 'response'
      }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  public updateEstado(id, value, venta): Observable<any> {
    venta.value = value;
    const data: any = venta;
    return this.http.post(`${environment.url}ventas`, data, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  public getReportes(desde, hasta, estado): Observable<any> {
    return this.http.get(`${environment.url}reportes?desde=${desde}&hasta=${hasta}&estado=${estado}`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  public getUsers(): Observable<any> {
    return this.http.get(`${environment.url}user`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  public redimir(id, puntos): Observable<any> {
    return this.http.post(`${environment.url}user`, {id, puntos}, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  public addUser(data): Observable<any> {
    return this.http.post(`${environment.url}users`, data, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<any>) => response.body));
  }

  public uploadFile(fileToUpload: File) {
    const _formData = new FormData();
    _formData.append('excel', fileToUpload, fileToUpload.name);
    return this.http.post(`${environment.url}addUser`, _formData);
  }
}
