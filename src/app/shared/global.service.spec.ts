import { TestBed } from '@angular/core/testing';

import { GLOBALService } from './global.service';

describe('GLOBALService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GLOBALService = TestBed.get(GLOBALService);
    expect(service).toBeTruthy();
  });
});
