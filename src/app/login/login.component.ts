import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {GLOBALService} from '../shared/global.service';
import {MzToastService} from 'ngx-materialize';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public icon = 'visibility_off';
  public type = 'password';
  public LoginForm: FormGroup;

  constructor(private router: Router, private toastService: MzToastService, public globalService: GLOBALService) {
    if (localStorage.getItem('user') !== null) {
      router.navigate(['/main']);
    }
    this.LoginForm = new FormGroup({
      correo: new FormControl('', Validators.required),
      contrasena: new FormControl('', Validators.required),
    });
  }

  public auth(form: FormGroup) {
    this.globalService.login(form.value).subscribe((response: any) => {
      localStorage.setItem('user', JSON.stringify(response.user));
      localStorage.setItem('ventas', JSON.stringify(response.ventas));
      this.router.navigate(['/main']);
    }, error1 => {
      this.toastService.show(error1.error.message, 4000, 'red');
    });
  }

  public showPassword() {
    if (this.type === 'password') {
      this.icon = 'visibility_on';
      this.type = 'text';
    } else {
      this.icon = 'visibility_off';
      this.type = 'password';
    }
  }

  ngOnInit() {
  }

}
